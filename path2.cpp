/**
 * From the OpenGL Programming wikibook: http://en.wikibooks.org/wiki/OpenGL_Programming
 * Used the obj loader and how to use it with OpenGL
 * This file is in the public domain.
 * Contributors: Sylvain Beucler
 */
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include "shader_utils.h"
#include "Space.hpp"

int screen_width=800, screen_height=600;
GLuint shaderProgram;
GLint attribute_v_coord = -1;
GLint attribute_v_normal = -1;
GLint uniform_m = -1, uniform_v = -1, uniform_p = -1, uniform_c = -1;
GLint uniform_m_3x3_inv_transp = -1, uniform_v_inv = -1;

bool move = false;
int prevX = 0, prevY = 0;
double mSense = 1.0;
glm::mat4 tCamera;
int last_ticks = 0;

//int cnt = 0;

double moveSpeed = 2.0;

static unsigned int fps_start = glutGet(GLUT_ELAPSED_TIME);
static unsigned int fps_frames = 0;
int prevTime = 0;
double dt = 0;

Space space;
bool follow = false;
std::vector<Node*> path;
Node* curNode;
int curNodeIndex = 1; //Assuming 0 is start
bool dcon = false;
bool dnodes = false;
bool smooth = false;
bool tighten = false;
glm::vec3 startPos;
std::vector<glm::vec3> endPos;
CSpace c;

//-Z is forward in OpenGL, so use this to create position vectors
glm::vec3 createVec3(double x, double y, double z) {
    return glm::vec3(x, y, -z);
}

void load_obj(const char* filename, Object* mesh) {
    std::ifstream in(filename, std::ios::in);
    if(!in) {
        std::cerr << "Cannot open " << filename << std::endl;
        exit(1);
    }
    std::vector<int> nb_seen;

    std::string line;
    while(getline(in, line)) {
        if(line.substr(0,2) == "v ") {
            std::istringstream s(line.substr(2));
            glm::vec4 v;
            s >> v.x;
            s >> v.y;
            s >> v.z;
            v.w = 1.0;
            mesh->vertices.push_back(v);    //Assuming objects are alligned -Z forward, so no conversion
        }
        else if(line.substr(0,2) == "f ") {
            std::istringstream s(line.substr(2));
            GLushort a,b,c;
            GLushort vn;
            char s1, s2;
            s >> a >> s1 >> s2 >> vn;
            s >> b >> s1 >> s2 >> vn;
            s >> c >> s1 >> s2 >> vn;
            a--;
            b--;
            c--;
            mesh->elements.push_back(a);
            mesh->elements.push_back(b);
            mesh->elements.push_back(c);
        }
        else if(line[0] == '#') {
            /* ignoring this line */
        }
        else {
            /* ignoring this line */
        }
    }

    mesh->normals.resize(mesh->vertices.size(), glm::vec3(0.0, 0.0, 0.0));
    nb_seen.resize(mesh->vertices.size(), 0);
    for(unsigned int i = 0; i < mesh->elements.size(); i+=3) {
        GLushort ia = mesh->elements[i];
        GLushort ib = mesh->elements[i+1];
        GLushort ic = mesh->elements[i+2];
        glm::vec3 normal = glm::normalize(glm::cross(
            glm::vec3(mesh->vertices[ib]) - glm::vec3(mesh->vertices[ia]),
            glm::vec3(mesh->vertices[ic]) - glm::vec3(mesh->vertices[ia])));

        int v[3];  v[0] = ia;  v[1] = ib;  v[2] = ic;
        for(int j = 0; j < 3; j++) {
            GLushort cur_v = v[j];
            nb_seen[cur_v]++;
            if(nb_seen[cur_v] == 1) {
                mesh->normals[cur_v] = normal;
            }
            else {
                // average
                mesh->normals[cur_v].x = mesh->normals[cur_v].x * (1.0 - 1.0/nb_seen[cur_v]) + normal.x * 1.0/nb_seen[cur_v];
                mesh->normals[cur_v].y = mesh->normals[cur_v].y * (1.0 - 1.0/nb_seen[cur_v]) + normal.y * 1.0/nb_seen[cur_v];
                mesh->normals[cur_v].z = mesh->normals[cur_v].z * (1.0 - 1.0/nb_seen[cur_v]) + normal.z * 1.0/nb_seen[cur_v];
                mesh->normals[cur_v] = glm::normalize(mesh->normals[cur_v]);
            }
        }
    }
}

int init_resources(char* vshader_filename, char* fshader_filename) {
    //Object *object = new Object();
    Object* agent = new Object();

    load_obj("models/agent.obj", agent);
    agent->radius = 0.5;
    agent->pos = createVec3(0, 0, -9);
    agent->model = glm::translate(agent->model, createVec3(0.0, 0.0, -9.0));
    agent->name = "agent";
    agent->color = glm::vec4(1.0, 0.0, 0.0, 1.0);
    
    Object* agent2 = new Object();
    load_obj("models/agent.obj", agent2);
    agent2->radius = 0.5;
    agent2->pos = createVec3(0, 0, 7.95);
    agent2->model = glm::translate(agent2->model, createVec3(0.0, 0.0, 7.95));
    agent2->name = "agent";
    agent2->color = glm::vec4(1.0, 0.0, 0.0, 1.0);
    Object* agent3 = new Object();
    load_obj("models/agent.obj", agent3);
    agent3->radius = 0.5;
    agent3->pos = createVec3(-1.05, 0, 7.95);
    agent3->model = glm::translate(agent3->model, createVec3(-1.05, 0.0, 7.95));
    agent3->name = "agent";
    agent3->color = glm::vec4(1.0, 0.0, 0.0, 1.0);
    Object* agent4 = new Object();
    load_obj("models/agent.obj", agent4);
    agent4->radius = 0.5;
    agent4->pos = createVec3(1.05, 0, 7.95);
    agent4->model = glm::translate(agent4->model, createVec3(1.05, 0.0, 7.95));
    agent4->name = "agent";
    agent4->color = glm::vec4(1.0, 0.0, 0.0, 1.0);
    Object* agent5 = new Object();
    load_obj("models/agent.obj", agent5);
    agent5->radius = 0.5;
    agent5->pos = createVec3(0, 0, 9);
    agent5->model = glm::translate(agent5->model, createVec3(0.0, 0.0, 9.0));
    agent5->name = "agent";
    agent5->color = glm::vec4(1.0, 0.0, 0.0, 1.0);
    Object* agent6 = new Object();
    load_obj("models/agent.obj", agent6);
    agent6->radius = 0.5;
    agent6->pos = createVec3(-1.05, 0, 9);
    agent6->model = glm::translate(agent6->model, createVec3(-1.05, 0.0, 9.0));
    agent6->name = "agent";
    agent6->color = glm::vec4(1.0, 0.0, 0.0, 1.0);
    Object* agent7 = new Object();
    load_obj("models/agent.obj", agent7);
    agent7->radius = 0.5;
    agent7->pos = createVec3(1.05, 0, 9);
    agent7->model = glm::translate(agent7->model, createVec3(1.05, 0.0, 9.0));
    agent7->name = "agent";
    agent7->color = glm::vec4(1.0, 0.0, 0.0, 1.0);

    Object *ground = new Object();
    load_obj("models/ground.obj", ground);
    ground->model = glm::scale(ground->model, glm::vec3(10.0f, 1.0f, 10.0f));
    ground->name = "ground";
    ground->color = glm::vec4(.71, .32, .17, 1.0);

    Object* node = new Object();
    load_obj("models/ground.obj", node);
    node->model = glm::scale(node->model, glm::vec3(0.2, 0.2, 0.2));
    node->name = "node";
    node->color = glm::vec4(0.0, 1.0, 0.0, 1.0);

    agent->upload();
    agent2->upload();
    agent3->upload();
    agent4->upload();
    agent5->upload();
    agent6->upload();
    agent7->upload();
    ground->upload();
    node->upload();
    
    Agent* a1 = new Agent(agent);
    a1->speed = moveSpeed*1.5;
    Agent* a2 = new Agent(agent2);
    a2->speed = moveSpeed;
    Agent* a3 = new Agent(agent3);
    a3->speed = moveSpeed;
    Agent* a4 = new Agent(agent4);
    a4->speed = moveSpeed;
    Agent* a5 = new Agent(agent5);
    a5->speed = moveSpeed;
    Agent* a6 = new Agent(agent6);
    a6->speed = moveSpeed;
    Agent* a7 = new Agent(agent7);
    a7->speed = moveSpeed;
    
    
    space.agents.push_back(a1);
    space.agents.push_back(a2);
    space.agents.push_back(a3);
    space.agents.push_back(a4);
    space.agents.push_back(a5);
    space.agents.push_back(a6);
    space.agents.push_back(a7);
    
    space.objects.push_back(ground);
    space.node = node;

    space.gridResolution = glm::vec2(200, 200);
    space.nodeCount = 500;
    space.nborCount = 15;

    //startPos = createVec3(-9, 0, -9);
    endPos.push_back(createVec3(0, 0, 9));
    endPos.push_back(createVec3(0, 0, -9));
    c = space.getCSpace();
    //space.c = &c;
    Graph g = c.getGraphPRM();
    space.g = g;
    //Make test path for testing ability to follow
    //path.push_back(new Node(createVec3(-9, 0, -9)));
    //path.push_back(new Node(createVec3(-6, 0, -3)));
    //path.push_back(new Node(createVec3(-4, 0, 1)));
    //path.push_back(new Node(createVec3(0, 0, 4)));
    //path.push_back(new Node(createVec3(4, 0, 7)));
    //path.push_back(new Node(createVec3(9, 0, 9)));

    //path = g.nodes;
    //path = g.pathBFS();
    //curNode = path[curNodeIndex];



    /* Compile and link shaders */
    GLint link_ok = GL_FALSE;
    GLint validate_ok = GL_FALSE;

    GLuint vs, fs;
    if(!(vs = create_shader(vshader_filename, GL_VERTEX_SHADER))) {
        return 0;
    }
    if(!(fs = create_shader(fshader_filename, GL_FRAGMENT_SHADER))) {
        return 0;
    }

    shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vs);
    glAttachShader(shaderProgram, fs);
    glLinkProgram(shaderProgram);
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &link_ok);
    if(!link_ok) {
        fprintf(stderr, "glLinkProgram:");
        print_log(shaderProgram);
        return 0;
    }
    glValidateProgram(shaderProgram);
    glGetProgramiv(shaderProgram, GL_VALIDATE_STATUS, &validate_ok);
    if(!validate_ok) {
        fprintf(stderr, "glValidateProgram:");
        print_log(shaderProgram);
    }

    const char* attribute_name;
    attribute_name = "v_coord";
    attribute_v_coord = glGetAttribLocation(shaderProgram, attribute_name);
    if(attribute_v_coord == -1) {
        fprintf(stderr, "Could not bind attribute %s\n", attribute_name);
        return 0;
    }
    attribute_name = "v_normal";
    attribute_v_normal = glGetAttribLocation(shaderProgram, attribute_name);
    if(attribute_v_normal == -1) {
        fprintf(stderr, "Could not bind attribute %s\n", attribute_name);
        return 0;
    }
    const char* uniform_name;
    uniform_name = "m";
    uniform_m = glGetUniformLocation(shaderProgram, uniform_name);
    if(uniform_m == -1) {
        fprintf(stderr, "Could not bind uniform %s\n", uniform_name);
        return 0;
    }
    uniform_name = "v";
    uniform_v = glGetUniformLocation(shaderProgram, uniform_name);
    if(uniform_v == -1) {
        fprintf(stderr, "Could not bind uniform %s\n", uniform_name);
        return 0;
    }
    uniform_name = "p";
    uniform_p = glGetUniformLocation(shaderProgram, uniform_name);
    if(uniform_p == -1) {
        fprintf(stderr, "Could not bind uniform %s\n", uniform_name);
        return 0;
    }
    uniform_name = "m_3x3_inv_transp";
    uniform_m_3x3_inv_transp = glGetUniformLocation(shaderProgram, uniform_name);
    if(uniform_m_3x3_inv_transp == -1) {
        fprintf(stderr, "Could not bind uniform %s\n", uniform_name);
        return 0;
    }
    uniform_name = "v_inv";
    uniform_v_inv = glGetUniformLocation(shaderProgram, uniform_name);
    if(uniform_v_inv == -1) {
        fprintf(stderr, "Could not bind uniform %s\n", uniform_name);
        return 0;
    }
    uniform_name = "color";
    uniform_c = glGetUniformLocation(shaderProgram, uniform_name);
    if(uniform_c == -1) {
        fprintf(stderr, "Could not bind uniform %s\n", uniform_name);
        return 0;
    }

    fps_start = glutGet(GLUT_ELAPSED_TIME);

    return 1;
}

void init_view() {
    tCamera = glm::lookAt(
        glm::vec3(0.0,  25.0, 0.0),   // eye
        glm::vec3(0.0,  -1.0, 0.0),   // direction
        glm::vec3(0.0,  0.0, -1.0));  // up
}

void logic() {
    /* FPS count */
    fps_frames++;
    int curTime = glutGet(GLUT_ELAPSED_TIME);
    int delta_t = curTime - fps_start;
    if(delta_t > 1000) {
        double fps = 1000.0 * fps_frames / delta_t;
        char fpsString[15];
        snprintf(fpsString, 15, "%f", fps);
        glutSetWindowTitle(fpsString);
        fps_frames = 0;
        fps_start = glutGet(GLUT_ELAPSED_TIME);
    }
    dt = (curTime - prevTime) / 1000.0;
    prevTime = curTime;

    //Move agent
    if(follow) {
        //cnt++;
        //printf("%d\n", (int)space.agents.size());
        space.moveAgents(c, dt);
    }
    //if(follow && cnt == 2) {
    //    printf("cnt: %d\n", cnt);
    //    std::exit(0);
    //}
    
    // View
    glm::mat4 world2camera = tCamera;
    glUniformMatrix4fv(uniform_v, 1, GL_FALSE, glm::value_ptr(world2camera));
    glm::mat4 v_inv = glm::inverse(world2camera);
    glUniformMatrix4fv(uniform_v_inv, 1, GL_FALSE, glm::value_ptr(v_inv));
    // Projection
    glm::mat4 camera2screen = glm::perspective(45.0f, 1.0f*screen_width/screen_height, 0.1f, 100.0f);
    glUniformMatrix4fv(uniform_p, 1, GL_FALSE, glm::value_ptr(camera2screen));

    glutPostRedisplay();
}

void draw() {
    glClearColor(0.0, 0.2, 0.8, 1.0);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    glUseProgram(shaderProgram);

    space.draw();
    space.drawNodes(space.g.nodes, dnodes, dcon);
}

void onDisplay() {
    logic();
    draw();
    glutSwapBuffers();
}

void onMouse(int button, int state, int x, int y) {
    prevX = x;
    prevY = y;
    const double movement = 2;
    int mod = glutGetModifiers();
    if(mod == GLUT_ACTIVE_SHIFT) {
        /*
        if((button != GLUT_LEFT_BUTTON && button != GLUT_RIGHT_BUTTON) || state == GLUT_UP) {
            return;
        }
        printf("Moving Node\n");
        //Unproject
        GLdouble model[16];
        GLdouble proj[16];
        GLint view[4];
        glGetDoublev(GL_MODELVIEW_MATRIX, model);
        glGetDoublev(GL_PROJECTION_MATRIX, proj);
        glGetIntegerv(GL_VIEWPORT, view);
        double xpos1, ypos1, zpos1, xpos2, ypos2, zpos2;
        gluUnProject((double)x, view[3]-(double)y, 0, model, proj, view, &xpos1, &ypos1, &zpos1);
        gluUnProject((double)x, view[3]-(double)y, 1, model, proj, view, &xpos2, &ypos2, &zpos2);
        glm::vec3 p1(xpos1, ypos1, zpos1);
        glm::vec3 p2(xpos2, ypos2, zpos2);
        printf("P0: %f, %f, %f\n", p1.x, p1.y, p1.z);
        printf("P1: %f, %f, %f\n", p2.x, p2.y, p2.z);
        glm::vec3 mouseLine = glm::normalize(p2 - p1);
        //Intersect ray and plane along origin
        double n = glm::dot(mouseLine, glm::vec3(0, 1, 0));
        if(std::abs(n) < 0.00001) {
            return;
        }
        double t = glm::dot(-p1, glm::vec3(0, 1, 0)) / n;
        glm::vec3 mPoint = p1 + glm::vec3(mouseLine.x*t, mouseLine.y*t, mouseLine.z*t);
        if(mPoint.x > 10.0 || mPoint.x < -10.0 || mPoint.z > 10.0 || mPoint.z < -10.0) {
            return;
        }
        //Add start or goal
        if(button == GLUT_LEFT_BUTTON) { //start
            startPos = mPoint;
        }
        else if(button == GLUT_RIGHT_BUTTON) { //end
            endPos = mPoint;
        }
        CSpace c = space.getCSpace();
        Graph g = c.getGraphPRM();
        space.g = g;
        */
    }
    else if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
        move = true;
    }
    else if(button == 3) { //Scroll down
        tCamera = glm::translate(tCamera, glm::vec3(0, mSense*movement, 0));
    }
    else if(button == 4) { //Scroll up
        tCamera = glm::translate(tCamera, glm::vec3(0, -mSense*movement, 0));
    }
    else {
        move = false;
    }
}

void onMotion(int x, int y) {
    if(move) {
        int delX = x - prevX;
        int delY = y - prevY;
        tCamera = glm::translate(tCamera, glm::vec3(-mSense*delX, 0, -mSense*delY));
    }
    prevX = x;
    prevY = y;
}

void onReshape(int width, int height) {
    screen_width = width;
    screen_height = height;
    glViewport(0, 0, screen_width, screen_height);
}

void onKey(unsigned char key, int x, int y) {
    switch(key) {
        case 27: //escape
            exit(0);
        case 'f':
            follow = !follow;
            break;
        case 'n':
            dnodes = !dnodes;
            break;
        case 'm':
            dcon = !dcon;
            break;
        case 's':
            smooth = !smooth;
            break;
        case 't':
            tighten = !tighten;
            break;
        case '1':
            for(int i = 0; i < space.agents.size(); i++) {
                path = space.g.findPath(DFS, space.agents[i]->agent->pos, endPos[(i==0)?0:1]);
                space.agents[i]->path = path;
            }
            break;
        case '2':
            for(int i = 0; i < space.agents.size(); i++) {
                path = space.g.findPath(BFS, space.agents[i]->agent->pos, endPos[(i==0)?0:1]);
                space.agents[i]->path = path;
            }
            break;
        case '3':
            for(int i = 0; i < space.agents.size(); i++) {
                path = space.g.findPath(UCS, space.agents[i]->agent->pos, endPos[(i==0)?0:1]);
                space.agents[i]->path = path;
            }
            break;
        case '4':
            for(int i = 0; i < space.agents.size(); i++) {
                path = space.g.findPath(GBFS, space.agents[i]->agent->pos, endPos[(i==0)?0:1]);
                space.agents[i]->path = path;
            }
            break;
        case '5':
            for(int i = 0; i < space.agents.size(); i++) {
                path = space.g.findPath(ASTAR, space.agents[i]->agent->pos, endPos[(i==0)?0:1]);
                space.agents[i]->path = path;
            }
            break;
    }
}

void free_resources() {
    glDeleteProgram(shaderProgram);
}


int main(int argc, char* argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA|GLUT_ALPHA|GLUT_DOUBLE|GLUT_DEPTH);
    glutInitWindowSize(screen_width, screen_height);
    glutCreateWindow("FPS: 0");

    GLenum glew_status = glewInit();
    if(glew_status != GLEW_OK) {
        fprintf(stderr, "Error: %s\n", glewGetErrorString(glew_status));
        return 1;
    }

    if(!GLEW_VERSION_2_0) {
        fprintf(stderr, "Error: your graphic card does not support OpenGL 2.0\n");
        return 1;
    }
    char* vglsl = (char*)"phong_vertex.glsl"; //Needed to get around depricated conversion
    char* fglsl = (char*)"phong_fragment.glsl";
    if(init_resources(vglsl, fglsl)) {
        init_view();
        glutDisplayFunc(onDisplay);
        glutMouseFunc(onMouse);
        glutMotionFunc(onMotion);
        glutReshapeFunc(onReshape);
        glutKeyboardFunc(onKey);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_DEPTH_TEST);
        last_ticks = glutGet(GLUT_ELAPSED_TIME);
        glutMainLoop();
    }

    free_resources();
    return 0;
}
