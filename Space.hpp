#ifndef SPACE_HPP_
#define SPACE_HPP_

#include <vector>
#include <cmath>
#include <deque>
#include <queue>
#include <GL/glew.h>   //Include order can matter here
#include <GL/freeglut.h>
//#include <SDL2/SDL.h>
//#include <SDL2/SDL_opengl.h>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>

enum Pathing {
    DFS,
    BFS,
    UCS,
    GBFS,
    ASTAR
};

///Implementation of mesh from https://gitlab.com/wikibooks-opengl/modern-tutorials/blob/master/obj-viewer/obj-viewer.cpp
class Object {
public:
    GLuint vbo_vertices, vbo_normals, ibo_elements;
    std::vector<glm::vec4> vertices;
    std::vector<glm::vec3> normals;
    std::vector<GLushort> elements;
    double radius;
    glm::vec3 pos;
    glm::mat4 model;
    std::string name;
    glm::vec4 color;

    Object() : vbo_vertices(0), vbo_normals(0), ibo_elements(0), radius(0), pos(glm::vec3(0)), model(glm::mat4(1)), name("") {}
    ~Object();
    void upload();
    void draw();
};

class Node {
public:
    Node(glm::vec3 pos): pos(pos), parent(NULL), cost(0) {}
    friend bool operator<(const Node& lhs, const Node& rhs);
    glm::vec3 pos;
    std::vector<Node*> conList;

    //used for the searches
    Node* parent;
    double cost;
};

//Comparison struct for use in the priority queue so it works with node pointers
struct CmpNodes {
    bool operator()(const Node* lhs, const Node* rhs) const {
        return lhs->cost > rhs->cost; //greater than because stl using a max heap
    }
};

class Graph {
public:
    std::vector<Node*> findPath(enum Pathing type, glm::vec3 start, glm::vec3 end);
    //Helpers
    std::vector<Node*> pathDFS(Node* n, std::vector<Node*> path, Node* start, Node* end);
    std::vector<Node*> pathBFS(Node* start, Node* end);
    std::vector<Node*> pathUCS(Node* start, Node* end);
    std::vector<Node*> pathGBFS(Node* start, Node* end);
    std::vector<Node*> pathAStar(Node* start, Node* end);
    bool sameNode(Node* n1, Node* n2);
    std::vector<Node*> makePath(Node* end);
    bool contains(Node* n, std::deque<Node*> closedList);
    double getGCost(Node* n, Node* m);
    double getHCost(Node* n, Node* end);

    std::vector<Node*> nodes;
    int nborCount;
};

class CSpace {
public:
    CSpace();
    CSpace(glm::vec2 res);
    CSpace(glm::vec2 res, int count);
    CSpace(glm::vec2 res, int count, int neighborCount);
    Graph getGraphPRM();
    bool isPathClear(double x0, double y0, double x1, double y1);
    void tightenPath(std::vector<Node*> &path);

    int nodeCount;
    int nborCount;
    int** cspace;
    glm::vec3 pos;
    glm::vec2 wh;
    glm::vec2 resolution;
};

class Agent {
public:
    Object* agent;
    std::vector<Node*> path;
    //Node* curNode;
    int pathIndex;
    bool smooth;
    bool tighten;
    float speed;
    glm::vec3 vel;
    std::vector<int> pastAgents;
    glm::vec3 avoidF;
    
    Agent(): agent(0), pathIndex(1), smooth(true), tighten(false), speed(2) {}
    Agent(Object* o): agent(o), pathIndex(1), smooth(true), tighten(false), speed(2) {}
    Agent(bool s, bool t): agent(0), pathIndex(1), smooth(s), tighten(t), speed(2) {}
    void moveAgent(CSpace c, double dt, std::vector<Agent*> agents, int thisAgent, std::vector<Object*> obst);
    void ttc(std::vector<Agent*> agents, int thisAgent, std::vector<Object*> obst);
    bool notVisited(std::vector<int> past, int index);
    void draw();
    void upload();
};

class Space {
public:
    std::vector<Agent*> agents;
    std::vector<Object*> obstacles;
    std::vector<Object*> objects;
    Object* node;
    Object* connection;
    Graph g;
    glm::vec2 gridResolution;
    int nodeCount;
    int nborCount;

    void upload();
    void draw();
    void drawNodes(std::vector<Node*> allNodes, bool drawNodes = false, bool drawConnection = false);
    CSpace getCSpace();
    void moveAgents(CSpace c, double dt);
};

#endif
